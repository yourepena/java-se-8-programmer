### Links úteis
* https://education.oracle.com/java-se-8-programmer-i/pexam_1Z0-808
* https://education.oracle.com/java-se-8-programmer-ii/pexam_1Z0-809
* https://learn.oracle.com/education/downloads/Exam808_SampleQuestion.pdf
* https://learn.oracle.com/education/downloads/Exam809_SampleQuestion.pdf
### Java Basics
- [X] Define the scope of variables
- [X] Define the structure of a Java class
- [X] Create executable Java applications with a main method
- [X] Import other Java packages to make them accessible in your code
- [x] Run a Java program from the command line; including console output
- [x] Compare and contrast the features and components of Java such as: platform independence, object orientation, encapsulation, etc

### Working With Java Data Types
- [X] Declare and initialize variables
- [X] Differentiate between object reference variables and primitive variables
- [x] Read or write to object fields
- [x] Explain an Object's Lifecycle (creation, "dereference" and garbage collection)
- [x] Call methods on objects
- [X] Manipulate data using the StringBuilder class and its methods ```*```
- [X] Creating and manipulating Strings
- [x] Develop code that uses wrapper classes such as Boolean, Double, and Integer. ```*```

### Using Operators and Decision Constructs
- [X] Use Java operators
- [X] Use parenthesis to override operator precedence
- [X] Test equality between Strings and other objects using == and equals ()
- [X] Create if and if/else constructs
- [X] Use a switch statement

### Using Operators and Decision Constructs
- [x] Creating and Using Arrays
- [x] Declare, instantiate, initialize and use a onedimensional array
- [x] Declare, instantiate, initialize and use multidimensional array
- [x] Declare and use an ArrayList

### Using Loop Constructs
- [x] Create and use while loops
- [x] Create and use for loops including the enhanced for loop
- [x] Create and use do/while loops
- [x] Compare loop constructs
- [x] Use break and continue

### Working with Methods and Encapsulation
- [x] Create methods with arguments and return values
- [x] Apply the static keyword to methods and fields
- [x] Create an overloaded method
- [x] Differentiate between default and user defined
- [x] constructors
- [x] Create and overload constructors
- [x] Apply access modifiers
- [x] Apply encapsulation principles to a class
- [x] Determine the effect upon object references and
- [x] primitive values when they are passed into methods that change the values

### Working with Inheritance
- [x] Implement inheritance
- [x] Develop code that demonstrates the use of polymorphism
- [x] Differentiate between the type of a reference and the
- [x] type of an object
- [x] Determine when casting is necessary
- [x] Use super and this to access objects and constructors
- [x] Use abstract classes and interfaces

### Handling Exceptions
- [x] Differentiate among checked exceptions,
- [x] RuntimeExceptions and Errors
- [x] Create a try-catch block and determine how exceptions alter normal program flow
- [x] Describe what Exceptions are used for in Java
- [x] Invoke a method that throws an exception
- [x] Recognize common exception classes and categories

### Working with Selected Classes from the Java API
- [x] Create and manipulate calendar data using classes from java.time.LocalDateTime, java.time.LocalDate, java.time.LocalTime, java.time.format.DateTimeFormatter,java.time.Period
- [x] Write a simple Lambda expression that consumes a
- [x] Lambda Predicate expression

## Legenda
```*```  Achei fraco este capítulo

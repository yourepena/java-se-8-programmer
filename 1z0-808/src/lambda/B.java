package lambda;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneOffset;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

class B {

	public static void main(String[] args) {
		
	  long x = Duration.between(LocalDateTime.now().toInstant(ZoneOffset.UTC), LocalDateTime.of(LocalDate.now(), LocalTime.now()).toInstant(ZoneOffset.UTC)).getSeconds();
	  System.out.println(x);
	}
	 
}

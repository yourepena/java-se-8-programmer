package lambda;

import java.util.ArrayList;
import java.util.List;

public class PersonFilter {

	public static List<Person> filter(List<Person> input, Matcher<Person> matcher) {
		List<Person> outPut = new ArrayList<Person>();
		for (Person t : input) {
			if (matcher.test(t))
				outPut.add(t);
		}
		return outPut;
	}

}

### Tipos de comentários

```java
// single line comment
/*
 multiple line
 comment
*/
class /* middle of line comment */ Person {
 /**
  * JavaDoc, starts with slash, then two *
  * and it is multiple line
  */
 public void method() { // single line comment again
 }
}
```

### Nome dos membros
 Podemos ter membros de tipos diferentes com o mesmo nome. Fique atento, o código a seguir compila normalmente:
```java 
class B {
 String b;

 B() {
 }

 String b() {
  return null;
 }
}
```
### Multíplas estruturas em um arquivo
Em Java, é possível definir mais de uma classe/interface em um mesmo arquivo, embora devamos seguir algumas regras:
 - Podem ser definidos em qualquer ordem;
 - Se existir alguma classe/interface pública, o nome do arquivo deve ser o mesmo dessa classe/interface;
 - Só pode existir uma classe/interface pública por arquivo;
 - Se não houver nenhuma classe/interface pública, o arquivo pode ter qualquer nome.
 - Logo, são válidos:
```java
// file1.java
interface First {}
class Second {}
// Third.java
public class Third {}
interface Fourth {} 
```

### Os seguintes exemplos são métodos main válidos:
```java
// parameter: array
public static void main (String[] args) {}
// parameter: varargs
public static void main (String... args) {}
// static public/public static are ok
static public void main(String[] args) {}
// parameter name does not matter
public static void main (String... 
listOfArgumentsOurUserSentUs){}
// parameter: array variation
public static void main (String args[]) {}
```

### Na hora da compilação, é possível definir em que versão do Java o código-fonte foi escrito. Isso é feito com a opção - source do comando javac ( ).
```
$ javac MyClass.java - source 1.3
$ javac certification/Test.java
$ java certification.Test
$ java -Dkey1=abc -Dkey2=def Foo xpto bar
$ javac -cp /path/to/library.jar:/another/path/ certification/Test.java
$ java -cp /path/to/library.jar:/another/path/ certification.Test
$ jar -cf library.jar certification
```

### Um ponto que exige atenção extra é que o manifest deve possuir uma última linha vazia. Sem esta linha, o manifest não é adicionado e não conseguimos executar o jar.
```
$ jar -cfm bib.jar mymanifest certification
```

### Caso tenhamos um import específico e um genérico, o Java usa o específico:
```java
import java.util.*;
import java.sql.Date;
class Test {
 Date d1; // java.sql
 Date d2; // java.sql
}
```

### Nenhuma classe de pacote que não seja o padrão pode importar uma classe do pacote padrão.
```java
class Manager {
}
//O arquivo model/Bank.java jamais compilará:
package model;
class Bank {
 Manager manager; // compilation error
}
```

### Em Java, não podemos importar todas as classes de subpacotes usando *

### Tanto quando declaradas como variáveis membro, ou quando arrays são criadas, os valores default para as variáveis são:
- Primitivos numéricos inteiros ─ 0
- Primitivos numéricos com ponto flutuante ─ 0.0
- Boolean ─ false
- Char ─ vazio, equivalente a 0
- Referências ─ null

### Os tipos inteiros têm os seguintes tamanhos:
- byte ─ 1 byte (8 bits, de -128 a 127);
- short ─ 2 bytes (16 bits, de –32.768 a 32.767);
- char ─ 2 bytes (só positivo), (16 bits, de 0 a 65.535);
- int ─ 4 bytes (32 bits, de –2.147.483.648 a 2.147.483.647);
- long  ─ 8 bytes (64 bits, de –9.223.372.036.854.775.808 a 9.223.372.036.854.775.807)
 
### Calculando o intervalo de valores
- Dado o número de bits N do tipo primitivo inteiro, para saber os valores que ele aceita, usamos a seguinte conta:
    - -2^(n-1) a 2^(n-1) -1
    - Tipo char: 0 a 2^(16) -1
- Os tipos de ponto flutuante têm os seguintes tamanhos em notação científica: 
    - float ─ 4 bytes (32 bits, de +/–1.4  *  10 ^ 45 a +/– 3.4028235  *  10 ^ 38);
    - double ─ 8 bytes (64 bits, de +/–4.9  *  10 ^ 324 a +/– 1.7976931348623157  *  10 ^ 308).
- Todos os números de ponto flutuante também podem assumir os seguintes valores:
    - +/– infinity
    - +/- 0
    - NaN (Not a Number)

### Int com bases diferentes
- Base octal: Precisa começar com 0 e os algarismo aceitos são de 0 a 7
- Base hexadecimal: Precisa começar com 0x ou 0X e os algarismo aceitos são de 0 a  16, mas como os algarismo de 10 a 16 não existe, podemos utilizar as letras de 'A' até 'F' maiúsculas ou minúsculas

### Underline em literiais
- int a = 123_456_789;

### Identificadores válidos devem seguir as regras:
- Não podem ser igual a uma palavra-chave;
- Podem usar letras (unicode), números,  $  e  _ ;
- O primeiro caractere não pode ser um número;
- Podem possuir qualquer número de caracteres.


### Objetos elegíveis x Objetos Coletados
O garbage collector roda em segundo plano juntamente com sua aplicação Java. Não é possível prever quando ele será executado, portanto, não se pode dizer com certeza quantos objetos foram efetivamente coletados em um certo ponto da aplicação. O que podemos determinar é quantos objetos são elegíveis para a coleta. A prova pode tentar se aproveitar do descuido do desenvolvedor aqui: nunca temos certeza de quantos objetos passaram pelo garbage collector, logo, somente indique quantos estão passíveis de serem coletados.
 
### Será dada a prioridade para o método que já podia existir antes no Java 1.4: 
```java
void method(int ... x) { } void method(int x) {} method(5); //Isso vai invocar o segundo método.

void method(int ... x) { } method(new int[] {1,2,3,4}); // Podemos passar um array de int 

void method(int[] x) { } method(1,2,3); // Mas nunca podemos chamar um método que recebe um array como se ele fosse varags 
```

### Podemos criar um objeto StringBuilder de diversas maneiras diferentes:
```java
StringBuilder sb1 = new StringBuilder(); // vazio 
StringBuilder sb2 = new StringBuilder("java"); // conteudo inicial
StringBuilder sb3 = new StringBuilder(50);  // tamanho inicial do array para colocar a string 
StringBuilder sb4 = new StringBuilder(sb2); // baseado em outro objeto do mesmo tipo
```

Tenha cuidado: ao definir o tamanho do array, não estamos criando uma String de tamanho definido, somente um array desse tamanho que será utilizado pelo StringBuilder , portanto: 
```java
StringBuilder sb3 = new StringBuilder(50); 
System.out.println(sb3); // linha em branco 
System.out.println(sb3.length()); // 0
```

### Observações Sobre StringBuffer/Builder 
- As classes StringBuffer e StringBuilder têm exatamente a mesma interface (mesmos métodos), sendo que a primeira é thread-safe e a última não (e foi adicionada no Java 5). Quando não há compartilhamento entre threads, use sempre que possível a StringBuilder , que é mais rápida por não precisar se preocupar com locks.```
- Cuidado, pois o método substring não altera o valor do seu StringBuilder ou StringBuffer , mas retorna a String que você deseja. Existe também o método subSequence que recebe o início e o fim e funciona da mesma maneira que o substring com dois argumentos.
- Os métodos substring do StringBuffer/Builder são imatáveis, retornando sempre uma nova string

### O código é interpretado da esquerda pra direita
```java
String value = 15 + 00 + " certification";
System.out.println(value); // 15 certification
```
### String 
- Uma string é tipo um array mas não tem properiedade length e sim um método chamado  length()
- Se tentamos acessar uma posição que não existe a execeção que dá é StringIndexOutOfBoundsException  
- O segredo do método susbtring é que ele não inclui o caractere da posição final, mas inclui o caractere da posição inicial:
```java
String text = "Java";
System.out.println(text.substring(1)); // ava
System.out.println(text.substring(-1)); // StringIndexOutOfBoundsException
System.out.println(text.substring(5)); // StringIndexOutOfBoundsException
System.out.println(text.substring(0, 4)); // Java
System.out.println(text.substring(1, 4)); // ava 
System.out.println(text.substring(0, 3)); // Jav
System.out.println(text.substring(0, 5)); // StringIndexOutOfBoundsException 
System.out.println(text.substring(-1, 4)); // StringIndexOutOfBoundsException
```

### Para o método compareTo:
- Compara duas strings pela ordem lexicografica  (vem de Comparable)
- Será negativo caso a String na qual o método for invocado vier antes; zero se for igual; positivo se vier depois do parâmetro passado.
```java
String text = "Certification";
System.out.println(text.equals("Certification")); // true
System.out.println(text.equals("certification")); // false
System.out.println(text.equalsIgnoreCase("CerTifIcatIon")); //true

System.out.println(text.compareTo("Aim")); // 2 
System.out.println(text.compareTo("Certification")); // 0 
System.out.println(text.compareTo("Guilherme")); // -4 
System.out.println(text.compareTo("certification")); // -32 
System.out.println(text.compareToIgnoreCase("certification"));//0

String text = "Pretendo fazer a prova de certificação de Java";
System.out.println(text.indexOf("Pretendo")); // imprime 0
System.out.println(text.indexOf("Pretendia")); // imprime -1 
System.out.println(text.indexOf("tendo")); // imprime 3 
System.out.println(text.indexOf("a")); // imprime 10 

System.out.println(text.lastIndexOf("a")); // imprime 45 
System.out.println(text.lastIndexOf("Pretendia")); //imprime -1 

System.out.println(text.startsWith("Pretendo")); // true 
System.out.println(text.startsWith("Pretendia")); // false 

System.out.println(text.endsWith("Java")); // true 
System.out.println(text.endsWith("Oracle")); // false
```

### A estrutura switch aceita várias opções, inclusive se estas estiverem separadas por quebra de linha:
```java
// Line n1
switch (cardVal) {
 case 4: case 5: case 6:
 case 7: case 8:
 System.out.println("Hit");
 break;
 case 9: case 10: case 11:
 System.out.println("Double");
 break;
 case 15: case 16:
 System.out.println("Surrender");
 break;
 default:
 System.out.println("Stand");
}
```

### O resultado é do tipo mais abrangente entre os das variáveis envolvidas ou, no mínimo, o int . 
```java
int age = 15;
long years = 5;
// ok, o maior tipo era long
long afterThoseYears = age + years;
// não compila, o maior tipo era long, devolve long 
int afterThoseYears2 = age + years; 

byte b = 1; short s = 2;
// devolve no mínimo int, compila 
int i = b + s;
// não compila, ele devolve no mínimo int 
byte b2 = i + s;
// compila forçando o casting, correndo risco de perder informação 
byte b2 = (byte) (i + s);
```

### Dividir (ou usar mod ) um inteiro por zero lança uma ArithmeticException. Se o operando for um float ou double , isso gera infinito positivo ou negativo (depende do sinal do operador). As classes Float e Double possuem constantes para esses valores.
```java
int i = 200;
int v = 0;
// compila, mas exception 
System.out.println(i / v);
// compila e roda, infinito positivo 
System.out.println(i / 0.0);

double positiveInfinity = 100 / 0.0;
double negativeInfinity = -100 / 0.0;
// número não definido (NaN)
System.out.println(positiveInfinity + negativeInfinity);
```
### Quando usamos pós-incremento, essa é a última coisa a ser executada. E quando usamos o pré-incremento, é sempre a primeira.
```java
int i = 10;
// 10, primeiro imprime, depois incrementa
System.out.println(i++);
// 11, valor já incrementado.
System.out.println(i); 
// 12, incrementa primeiro, depois imprime 
System.out.println(++i);
// 12, valor incrementado.
 System.out.println(i);
```

### Calcule o resultado
```java
// Pré
int a = 10;
a += ++a + a + ++a;
a = a + ++a + a + ++a;
a = 10 + ++a + a + ++a;
a = 10 + 11 + 11 + ++a;
a = 10 + 11 + 11 + 12;
a = 44;

// Pós
int j = 0;
int i = (j++ * j + j++);

i= (0 * j + j++ + j); // j = 1
i= (0 * 1 + j++ + j); // j = 1
i= (0 * 1 + 1 + 2); // j = 2
i = 3;

int a = 15, b = 20, c = 30;
a = b = c; // b = 30, portanto a = 30

int a = 15, b = 20, c = 30;
a = (b = c + 5) + 5; // c = 30, portanto b = 35, portanto a = 40
```

### Devemos tomar muito cuidado na concatenação de String e precedência:
```java
System.out.println(15 + 0 + " != 150");
// 15 != 150 
System.out.println(15 + (0 + " == 150"));
// 150 == 150
System.out.println(("guilherme" + " silveira").length());
// 18 
System.out.println("guilherme" + " silveira".length());
// guilherme9
```

### O Java mantém um pool de objetos do tipo String
```java
String name1 = "Mario";
String name2 = "Mario";
System.out.println(name1 == name2); // false

String s1 = "string";
String s2 = "string";
String s3 = new String("string"); 
System.out.println(s1 == s2); // true, mesma referencia
System.out.println(s1 == s3); // false, referências diferentes 
System.out.println(s1.equals(s3)); // true, mesmo conteúdo
```

###  Quando concatenamos literais, a String resultante também será colocada no pool. apenas usando literais em ambos os lados
```java
String ab = "a" + "b";
System.out.println("ab" == ab); // true

String a = "a";
String ab = a + "b"; //usando uma referência e um literal
System.out.println("ab" == ab); // false
```

### Objetos resultantes de retornos de métodos não são buscados no pool, são novos objetos:
```java
String str = "12 text 345678";
String txt1 = "text"; 
String txt2 = str.substring(3, 7);  // new string 
System.out.println(txt1 == txt2); // false
System.out.println(txt1.equals(x.substring(3, 7)); // true
```

### Se o retorno do método for exatamente o conteúdo atual do objeto, nenhum objeto novo é criado:
```java
String str = "HELLO WORLD";
String upper = str.toUpperCase(); // já está maiúscula 
String subs = str.substring(0,11); // string completa 
System.out.println(str == upper); // true 
System.out.println(str == subs); // true 
System.out.println(str == str.toString()); // true
```
### Quantas Strings são criadas?
```java
//Cria 2 objetos, um literal (que vai para o pool) e o outro com o new 
String h = new String ("hello "); 
//nenhum objeto criado, usa o mesmo do pool 
String h1 = "hello ";
//novo objeto criado e inserido no pool 
String w = "world";
//nenhum objeto criado, usa do pool 
System.out.println("hello ");
//criado um novo objeto resultante da concatenação, mas este não vai para o pool 
System.out.println(h1 + "world"); 
//Novo objeto criado e colocado no pool (Hello com H maiúsculo). 
System.out.println("Hello " == h1);
```

### Pegadinha marota
```java
if(false) {.... } //compila, apesar de ser unreachable code
```

### Em cada case , só podemos usar como valor um literal, uma variável final atribuída com valor literal, ou expressões envolvendo os dois. Nem mesmo null é permitido:
```java
int value = 20;
final int FIVE = 5; 
int thirty = 30;
 switch (value) { 
    case FIVE: // constante 
        System.out.println(5); 
    case 10: // literal 
        System.out.println(10); 
    case FIVE * 4: // operação com constante e literal 
        System.out.println(20); 
    case thirty: // compile error, variável
        System.out.println(30); 
    case thirty + FIVE: // compile error, operação envolvendo variável 
        System.out.println(35); 
    case null: // compile error, explicit null 
        System.out.println("null"); 
}
```

### Para ser considerada uma constante em um case , a variável, além de ser final, também deve ter sido inicializada durante a sua declaração. Inicializar a variável em outra linha faz com que ela não possa ser usada como valor em um case :
```java
int v = 10;
final int TEN = 10;
final int TWENTY; // final, mas não inicializada
TWENTY = 20; // inicializada
switch (v) {
case TEN:
    System.out.println("10!");
    break;
case TWENTY: // compile error
    System.out.println("20!");
    break;
}
```

### Um detalhe sobre a sintaxe do default é que ele pode aparecer antes de um ou de diversos case s. Desta forma:
```java
int option = 4;
switch (option) {
    case 1:
        System.out.println("1");
    case 2:
        System.out.println("2");
    default:
        System.out.println("DEFAULT");
    case 3:
        System.out.println("3");
}
```

### Podemos declarar um array da seguinte forma:
```java
// Declaração de um array para guardar variáveis do tipo long. 
long []size;
```

### Ao inicializar um array devemos declarar a capacidade dele (capacidade 0 é permitida). Ao inicializar um array as variaveis contidas são instanciadas implicitamente.
```java
int[] ages; ages = new int[10];
System.out.println(ages[0]); // Imprime 0
int[] numbers = new int[0]; // Compila e roda
int[] numbers2 = new int[-2]; // Compila mas lança uma NegativeArraySizeException
int[] numbers3 = new int[]{1,2,5,7,5};
```
### Se fizermos a declaração e a inicialização em linhas separadas, o código não compila:
```java
int[] numbers = {1,2,5,7,5}; // ok
int[] numbers2; 
numbers2 = {1,2,5,7,5}; // compile error
```
### Não há casting de arrays de tipo primitivo, portanto, não adianta tentar:
```java
int[] values = new int[10];
long[] vals = values; // compile error

// Um array de quatro dimensões.
int[] [][]hipercube[];

```
### Lembrando que quando adicionamos um tipo não primitivo dentro de um array não é criado um novo objeto somente é passada a referência, por tanto:
```java
Client guilherme = new Client();
guilherme.setName("Guilherme");
Client[] clients = new Clients[10];
clients[0] = guilherme;
System.out.println(guilherme.getName()); // Guilherme
System.out.println(clients[0].getName()); // Guilherme
guilherme.setName("Silveira");
System.out.println(guilherme.getName()); // Silveira
System.out.println(clients[0].getName()); // Silveira
```
### Podemos também inicializar somente a primeira dimensão, deixando as outras para depois:
```java
int[][][] cube = new int[10][][];
```

### Note que o remove remove somente a primeira ocorrência daquele objeto.

### Caso desejarmos um array de String, devemos indicar isso ao método toArray de duas formas diferentes:
```java
ArrayList<String> names = new ArrayList<String>(); 
names.add("certification"); 
names.add("java"); 
String[] names2 = names.toArray(new String[0]); 
String[] names3 = names.toArray(new String[names.size()]); 
```
Ambas passam um array de String: o primeiro menor e o segundo com o tamanho suficiente para os elementos. Se ele possui o tamanho suficiente, ele mesmo será usado; enquanto que, se o tamanho não é suficiente, o toArray cria um novo array do mesmo tipo.

### O método set , que serve para alterar o elemento em determinada posição:
```java
ArrayList<String> names = new ArrayList<String>(); 
names.add("java"); 
names.set(0, "certification"); 
System.out.println(names.get(0)); // certification 
System.out.println(names.size()); // 1
```

### FOR
Um laço for é construido por 3 parte, a declaração, a condição e a modificação. A declaração é executada somente uma vez, a condição é verificada cada vez que o loop executa e a modificação é executada no final de cada loop.
```java
//Mas podemos ter umas declaração de for da seguinte forma:
for(;;)
// Que seria a mesma coisa que 
for(;true;)
//Podemos ainda, realizar mais de uma atribuição
for(int i =0, j =0,p=1;;)
//a cada volta do laço, incrementamos o i e decrementamos o j 
for (int i=1,j=2;; i++,j--)
for (int i = 0; i < 10; System.out.println(i++)) { // bizarro // code }
```

### Não podemos utilizando o enhance for ou foreach, modificar o conteúdo da coleção que estamos percorrendo usando a variável que declaramos

### O laço do .. while sempre termina em ponto e vígula:
```java
int i = 1; do { System.out.println(i); i++; } while (i < 10) // não compila, faltou o ;
```

### O código a seguir imprime os valores de 1 a 10. Mas como ele compila sendo que temos uma URI logo antes do laço for ? 
```java
http://www.casadocodigo.com.br 
http://www.codecrushing.com 
for (int i = 1; i <= 10; i++) {
    System.out.println(i); 
    } 
}
```

### Rótulos podem ser repetidos desde que não exista conflito de escopo:
```java
void repeatedLabel() {
    myLabel: for (int i = 0; i < 10; i++) {
        break myLabel;
    }
    myLabel: for (int i = 0; i < 10; i++) {
        break myLabel;
    }
}

void sameNameNestedLabelsDoesNotCompile() {
    myLabel: for (int i = 0; i < 10; i++) {
        // compile error
        myLabel: for (int j = 0; j < 10; j++) {
            break myLabel;
        }
    }
}
```

### Não há conflito de nome entre rótulos e variáveis, pois seu uso é bem distinto. O compilador sabe se você está referenciando um rótulo ou uma variável:
```java
class A {
	int myLabel = 15;

	void labelNameAndVariableNameIsOk() {
		myLabel: for (int i = 0; i < 10; i++) {
			int myLabel = 10;
			break myLabel;
		}
	}
}
```

### Um mesmo statement pode ter dois labels:
```java
void twoLabelsInTheSameStatement() { first: second: for (int i = 0; i < 10; i++) { System.out.println(i); } }
```

### Outros modificadores da assinatura são opcionais e somente alguns são cobrados na prova:

- final ─ Em caso de herança, o método não pode ser sobrescrito nas classes filhas. 
- abstract ─ Obriga as classes filhas a implementarem o método. O método abstrato não pode ter corpo definido. 
- static ─ O método deixa de ser um membro da instância e passa a ser acessado diretamente através da classe. 
- synchronized ─ Não cai nesta prova. Lock da instância. 
- native ─ Não cai nesta prova. Permite a implementação do método em código nativo (JNI); 
- strictfp ─ Não cai nesta prova. Ativa o modo de portabilidade matemática para contas de ponto flutuante. throws <EXCEPTIONS> ─ Após a lista de parâmetros, podemos indicar quais exceptions podem ser jogadas pelo método.

### O valor de b será 0, e não 15, uma vez que a variável a ainda não foi inicializada e possui seu valor padrão da execução do método getMethod .
```java
static int b = getMethod(); public static int getMethod() { return a; } static int a = 15;
```

### Caso uma classe possua um método estático, ela não pode possuir outro método não estático com assinatura que a sobrescreveria (mesmo que em classe mãe/filha):

### Métodos sobrecarregados podem ter ou não um retorno diferente e uma visibilidade diferente. Mas eles não podem ter exatamente os mesmos tipos e quantidade de parâmetros. Nesse caso, seria uma sobrescrita de método.

### No seguinte exemplo o compilador fica perdido já que não sabe qual o tipo de dado está sendo passado na linha destacada:
```java
class Test {
	void method(int i, double x) {
	}

	void method(double x, int i) {
	}

	public static void main(String[] args) {
		new Test().method(2.0, 3); // double, int
		new Test().method(2, 3.0); // int, double
		new Test().method(2, 3); // compile error
	}
}
```

### Já se o método test for privado, como o binding da chamada ao método é feito em compilação, o método invocado pelo construtor é o da classe mãe, sem dar a Exception :
```java
class Base {
	String name;

	Base() {
		test();
		name = "guilherme";
	}

	private void test() {
		System.out.println("test");
	}
}

class Main extends Base {
	Main() {
		super();
	}
	 void test() {
		System.out.println(name.length());
	}

	public static void main(String[] args) {
		new Main();
	}
}
```

### Quando existem dois construtores na mesma classe, um construtor pode invocar o outro através da palavra chave this. 
```java
class Test {
	public Test() {
		System.out.println("simple");
	}

	public Test(int i) {
		this(); // simple
	}
}
```

### Os construtores e os métodos seguem as mesmas regras de sobrecarga, se existe um método/construtor com parâmetros vazios e outro com varargs o compilador irá invocar o vazio:
```java
clas Test {
    Test(){
        System.out.println("Empty");
    }
    Test(String... varargs) {
        System.out.println("Varargs");
    }
}
```

### A instrução this do construtor, caso presente, deve ser sempre a primeira dentro do construtor. Por consequência duas chamadas dá erro de compilação também
```java
class Test { 
	Test() {
		String value = "value..."; 
		this(value); // compile error
	} 
    Test(String a) { // sobrecarga
		this(value);
        this(value); // compile error
	} 
}
```
### A instrução this pode envolver outras instruções, mas não podem pertencer a mesma classe pois o objeto ainda não foi criado:
```java
class Test {
    Test(){
        this(imprimir());
    }
    Test(String a){
        System.out.println(a);
    }
    public static String imprimir(){
        return "Imprimir...";
    }
}
class Test {
    Test(){
        this(imprimir()); // Erro de compilção
    }
    Test(String a){
        System.out.println(a);
    }
    public String imprimir(){
        return "Imprimir...";
    }
}
```
 
### Modificadores de acesso:
- <b>Public:</b> Modificador mais abrangente, qualquer classe de qualquer pacote conseguirá chamar os membros (classes, construtores, métodos, variaveis) marcados com este modificador.
- <b>Protected:</b> Membros com este modificador serão visíveis pelas classes declaradas no mesmo pacote e pelas classes filhas. Uma observação importante é que se na classe filha tentarmos fazer um cast para a classe Pai a variavel protected não será mais visível.
- <b>Default:</b> Membros com este modificador serão visiveis somente pelas classes declaradas no mesmo pacote. Mesmo que seus membros sejam públicos não é possível importar nem realizar chamadas para seus membros fora do mesmo pacote. Classes não podem ser declaradas explícitamente com o modificador default
- <b>Private:</b> É o modificador mais restritivo de todos. Membros definidos com este modificador somente poderão ser acessados pela mesma classe. Classe top lever não podem ser declaradas com este modificador. Classes anônimas podem acessar atributos privados da classe onde estão contidas. Métodos privados não podem ser sobrecarregados, se tentar realizar esta operação na verdade somente será criado um novo método. 

### O heap é o "lugar" no qual são guardados os objetos criados durante a execução.
```java 
class Test {
	public static void main(String[] args) {
		int i = 2;
		test(i);
	}
	private static void test(int i) {
		for (int j = 0; j < i; j++) {
			new String("j = " + j);
		}
	}
}
```

### Existem duas variaveis independentes nos dois métodos (main e test). Mas como são variaveis não primitivas elas não copiam valor e sim são referências para o mesmo objeto, por tanto se alteramos qualquer variavel o objeto em comum será modificado também. Na prática parece que são as mesmas variaveis. 
```java
class Test {
	public static void main(String[] args) {
		Exam exam = new Exam();
		exam.timeLimit = 100;
		test(exam);
		System.out.println(exam.timeLimit);
	}

	static void test(Exam exam) {
		exam.timeLimit = 210;
	}
}
class Exam {
	double timeLimit;
}
```

### Para podermos herdar de uma classe, a classe mãe precisa ser visível pela classe filha e, pelo menos, um de seus construtores também. O exemplo a seguir não compila, pois existe um construtor padrão (default constructor) que chama o construtor sem argumentos da classe pai, que não existe:
```java
class Parent {
	Parent(int x) {}
}
class Child1 extends Parent{ 
	// compile error 
    // implicit Child1() { super(); } }
}
class Child2 extends Parent{ 
	 Child2() {
         super(15);
     }
}
```

### Não existe herança de métodos estáticos. Mas quando herdamos de uma classe com métodos estáticos, podemos chamar o método da classe mãe usando o nome da filha (embora não seja uma boa prática):
```java
class W {
	static void method() {
		System.out.println("w");
	}
}
class Z extends W {}
class Test {
	public static void main(String[] args) {
		W.method();
		Z.method();
	}
}
```
 
### Se o exemplo a seguir não fossem membros estáticos, a denifição de qual método chamar se daria em tempo de execução, porém como os métodos são estático a chamada se faz em tempo de compilação.
```java
class W {
	public static void method() {
		System.out.println("w");
	}
}
class Z extends W {
	public static void method() {
		System.out.println("z");
	}
}
public class Main {
	public static void main(String[] args) {
		W w = new W();
		w.method(); 
		Z z = new Z();
		z.method(); // z
		W zPolimorphedAsW = z;
		zPolimorphedAsW.method();
	}
}
```

### Dentro de um método estático não podemos chamar o super nem o this, pois não temos instância de um objeto e sim um membro de uma classe.
### Por não existir herança, em métodos estáticos não pode ser usado o atributo abstract.
### Não existe sobrescrita de atributos. Podemos, sim, ter um atributo na classe filha com mesmo nome da mãe, mas não chamamos de sobrescrita. Nesses casos, o objeto vai ter 2 atributos diferentes: um da mãe (acessível com super ) e um na filha (acessível com this ).
### A classe pai não pode ser final, mas a filha pode ser final;
### A classe filha consegue herdar todos os membros da classe pai, mas não necessariamente consegue visualziar.

#### Para reescrever um método, é necessário: 
- Exatamente o mesmo nome; 
- Os parâmetros têm de serem iguais em tipo e ordem (nomes das variáveis podem mudar); 
- Retorno do método deve ser igual ou mais específico que o da mãe; Visibilidade deve ser igual ou maior que o da mãe;
- Exceptions lançadas devem ser iguais ou menos que na mãe;
- Método na mãe não pode ser final .


### Muito cuidado com interfaces, pois a definição de um método é, por padrão, public , e o exercício pode apresentar uma pegadinha de compilação: 
```java
interface A {
	void a();
}
class B implements A {
	void a() {} // compile error
}
class C implements A {
	public void a() {} // ok
}
```

### Estranhamente, um método sobrescrito pode ser abstrato, dizendo para o compilador que quem herdar dessa classe terá de sobrescrever o método original:
```java
class A {
	void a() {
	}
}
abstract class B extends A {
	abstract void a(); // ok
}
class C extends B {
	// compile error
}
class D extends B {
	void a() {
		// ok
	}
}
```
### Ao usar polimorfismo podemos usar como referência uma classe pai para instanciar uma classe filha, os métodos sobreescritos pela classe filha, passarão fazer parte da refrência feita mas não o resto dos métodos.
```java
class Vehicle {
	void turnon() {
		System.out.println("Vehicle running");
	}
}
class Car extends Vehicle {
	void turnon() {
		System.out.println("Car running");
	}
	void turnoff() {
	}

    public static void main(String args) {
        Vehicle v = new Car();
        v.turnon(); // Car running
        v.turnoff(); // Compile error
    }
}
```
### Somente podemos acessar os métodos de acordo com o tipo da referência, pois a verificação da existência do método é feita em compilação. Mas qual o método que será invocado, isso será conferido dinamicamente, em execução.

### Exemplo de execução ciclica:
```java
class A {
	public void method() {
		System.out.println("a");
		this.method2();
	}
	public void method2() {
		System.out.println("parent method2");
	}
}
class Main extends A {
	public void method() {
		System.out.println("b");
		super.method();
	}
	public void method2() {
		System.out.println("c");
		method();
		super.method();
	}
	public static void main(String[] args) {
		new Main().method2();
	}
}
```

Podemos referenciar um objeto pelo seu próprio tipo, por uma de suas classes pai, ou por qualquer interface implementada por ele, direta ou indiretamente. Os exemplos a seguir compilam pois o compilador entende que o tipo da referência que está sendo atribuída sempre é do tipo da esquerda: 
```java
interface A {}
interface B {}
class C implements A {}
class D extends C implements B {}
public class Main {
	public static void main(String[] args) {
		D d = new D();
		C c = new D();
		C c2 = d;
		B b = new D();
		B b2 = d;
		A a = new D();
		A a2 = a;
	}
}
```

### Quando temos um método na classe pai privado ou default, não existe overwritten de métodos, já que a classe filha não sabe da existência desse método.

### Se você está subindo na hierarquia de classes, a autopromoção vai fazer tudo sozinho; e se você estiver descendo, vai precisar de casting. Se não houver um caminho possível, não compila nem com casting. Na prova, faça sempre os diagramas de hierarquia de tipos que fica extremamente fácil resolver esses castings.

### O código a seguir compila! Apenas com a possibilidade de existir uma classe dessa, o compilador já aceita aquele casting, mesmo que uma classe dessas não exista na prática. Tem uma exeção, caso a classe Car for final e não implementar Runnable o próprio compilador já acusa erro, pois não existe a possibilidade dela ter filhos.
```java
Car c = new Car();
Runnable r = (Runnable) c;
```

### O instanceof não compila se a referência em questão for obviamente incompatível, por exemplo:
```java
String s = "a";
boolean b = s instanceof java.util.List; // compile error
``` 
### Interfaces podem herdar de múltiplas interfaces _

### O operador instanceof ( variable instanceof ClassName ) devolve true caso a referência variable aponte para um objeto do tipo ou subtipo de ClassName .

### Em todos os casos, precisamos chamar o construtor da classe pai para a classe filha, todos. Se não escolhemos um construtor o compilador adiciona a chamada super().

### Atenção, a chamada do construtor com super ou this só pode aparecer como primeira instrução do construtor. Portanto, só podemos fazer uma chamada desses tipos.

### Lembre-se de que o binding de uma variável ao tipo é feito em compilação. Portanto, se tentarmos acessar a variável speed fora do Car através de uma referência a Car , o valor alterado é o da variável Car.speed : 
```java
class Vehicle {
	double speed = 30;
}
class Car extends Vehicle {
	double speed = 50;
	void print() {
		System.out.println(speed); // 1000
		System.out.println(this.speed); // 1000
		System.out.println(super.speed); // 30
	}
}
class Test {
	public static void main(String[] args) {
		Car c = new Car();
		c.speed = 1000;
		c.print();
	}
}
```
### E se fizermos o mesmo através de uma referência a Vehicle , alteramos a speed do Vehicle 
```java
class Vehicle {
	double speed = 30;
}
class Car extends Vehicle {
	double speed = 50;
	void print() {
		System.out.println(speed); // 50
		System.out.println(this.speed); // 50
		System.out.println(super.speed); // 1000
	}
}
class Test {
	public static void main(String[] args) {
		Car c = new Car();
		((Vehicle) c).speed = 1000;
	}
}
```
### Classe abstratas:
- Uma classe abstrata pode não ter nenhum método abstrato
- Se uma classe tem um método que é abstrato, ela deve ser declarada como abstrata, ou não compilará,
- Uma classe abstrata não pode ser instanciada diretamente
- Uma classe concreta que herde de uma classe abstrata precisa implementar os métodos da classe pai.
- Caso a subclasse seja abastrata os métodos não precisam ser implementados 

### O código de uma classe abstrata pode invocar métodos dela mesma, que ainda são abstratos, uma vez que ele só será executado quando o objeto for criado:
```java
abstract class X {
	void x() {
		System.out.println(y());
	}
	abstract String y();
}
class Y extends X {
	String y() {
		return "code";
	}
}
class Test {
	public static void main(String[] args) {
		new Y().x(); // code
	}
}
```
### Interfaces:
- Por padrão, são todos métodos públicos e abstratos.
- Uma classe concreta que implemente uma interface deverá implementar todos os métodos.
- Por todos os métodos serem públicos, a implementação não pode ser diferente.
- Podemos declarar variaveis e todas elas serão public static final.
- Podemos extender de várias interfaces, mas nunca implementar.

### Cuidado com as exeções específica para cada tipo de dado
- String StringIndexOutOfBoundsException
- Array ArrayIndexOutOfBoundsException
- List IndexOutOfBoundsException

### Exeções mais comuns
- Na inicialização de qualquer bloco estático, se acontecer alguma exeção o Java lança um ExceptionInInitializerError 
- Em loop infinitos StackOverflowError
- Em tempo de compilação, se deletamos uma classe o Java lança a seguinte execeção: NoClassDefFoundError
- Acabou a memória livre disponível OutOfMemoryError


### Existe diferença quando tentamos acessar uma posição que não existe em um array e quando tentamos fazer a mesma coisa numa lista. As exeções lançadas são ArrayIndexOutOfBoundsException e IndexOutBoundException respetivamente

## Java 8

### Printf e format
- São métodos iguais e servem para formatar strings. 
- A estrutra dos comandos é a seguinte %[index$][flags][width][.precisao]type. Onde tudo o que tiver entre []s não é obrigatório. 
- Os types sãp: s: String, d: Números Inteiros, n: Quebra de linha, c: char, b: boolean, f: Números decimais.
- Index seria a posição do array que será utilizado para substituir o valor.
- Width seria o tamanho da string, ele completa com espaços em brancos à esquerda até chegar no tamanho especifico.
- Flags:
	- Números negativos
		- "+" Sempre inclui um sinal de positivo (+) ou negativo (-) em números.
		- "(" Números negativos são exibidos entre parênteses. 
	- Dois de alinhamento à esquerda ou direita: 
		- "-" Alinha à esquerda. Precisa de tamanho para ser usado. 
		- "0" Completa a esquerda com zeros. Precisa de tamanho para ser usado.
	- Temos uma flag para separar casa de milhares e decimais: 
		- "," Habilita separadores de milhar e decimal.

### Classes Wrappers:
- As classes wrappers númericas possuem dois construtores, um esperando o valor primitivo e outro uma string que lança NumberFormatException caso a string não possa ser convertida para número;
- Character possui somente um constructor;
- Boolean tem dois construtores também, mas se a string passada não é true escrita de qualquer forma (ex. TruE) o resultado será false;
- As classes Boolean e Character só possuem métodos para converter para o próprio tipo primitivo (byteValue e charValue);
- Os Wrappers possuem métodos para converter string para o número primitivo (parseXXX). 
- Os Wrappers números possuem os mesmos métodos para converter de string para tipo primitivo, e ainda possuem uma sobrecarga com um segundo atributo para receber a base
- Os Wrappers possuem o método valueOf que irá converter uma string para um Wrapper
- Podemos ainda converter de Wrapper para string com o método toString(param) 
- As classes Long e Integer possuem um método mais direto para obter o número na base desejada (toBinaryString, toHexString, toOctalString);
- O Java, para economizar memória, mantém um cache de alguns objetos e, toda vez que é feito um boxing, ele os reutiliza. Os seguintes objetos são mantidos no cache: 
	- Todos Boolean e Byte;
	- Short e Integer de -128 até 127; 
	- Caracter ASCII, como letras, números etc. 
- Sempre que você encontrar comparações usando == , envolvendo wrappers, preste muita atenção aos valores: se forem baixos, é possível que o resultado seja true mesmo sendo objetos diferentes!


### Nova API de Datas Java 8 (java.time). As classes que serão cobradas são: 
- LocalDate : representa uma data sem hora no formato yyyy-MM-dd (ano-mês-dia). 
- LocalTime : representa uma hora no formato hh:mm:ss.zzz (hora:minuto:segundo.milissegundo). 
- LocalDateTime : representa uma data com hora no formato yyyy-MM-dd-HH-mm-ss.zzz (ano-mês-dia-horaminuto-segundo.milissegundo).
- MonthDay : representa um dia e mês, sem o ano. 
- YearMonth : representa um mês e ano, sem o dia. 
- Period : representa um período de tempo, em dia, mês e ano. 
- DateTimeFormatter : classe que possui vários métodos para formatação.

### Assim como a classe String todos os Objetos do pacote java.time são imutáveis. E podemos iniciarlizar da seguinte forma:
```java
LocalTime currentTime = LocalTime.now(); // 09:05:03.244
LocalDate today = LocalDate.now(); // 2014-12-10 
LocalDateTime now = LocalDateTime.now(); // 2014-12-10-09-05-03.244
```

### Padraozinho a ser implementado. Os nomes mais comuns são:
- get : obtém o valor de algo; 
- is : verifica se algo é verdadeiro; 
- with : lembra um setter, mas retorna um novo objeto com o valor alterado; 
- plus : soma alguma unidade ao objeto, retorna um novo objeto com o valor alterado; 
- minus : subtrai alguma unidade do objeto, retorna um novo objeto com o valor alterado; 
- to : converte um objeto de um tipo para outro; 
- at : combina um objeto com outro.

### Para obter alguma porção de uma data, podemos usar os métodos precedidos por get : 
```java
LocalDateTime now = LocalDateTime.of(2014,12,15,13,0);
System.out.println(now.getDayOfMonth()); // 15 
System.out.println(now.getDayOfYear()); // 349 
System.out.println(now.getHour()); // 13 
System.out.println(now.getMinute()); // 0 
System.out.println(now.getYear()); // 2014 
System.out.println(now.getDayOfWeek()); // MONDAY 
System.out.println(now.getMonthValue()); // 12 
System.out.println(now.getMonth()); // DECEMBER
```
### Lembrando que nem todas as classes possuem todos os métodos, como LocalDate e LocalTime estão incompletos, eles também terão ausentes esses métodos.

### Existe também um método get que retorna um inteiro e espera como parâmetro um objeto que implementa TemporalField, normalmente é o ChronoField.
```java
System.out.println(now.get(ChronoField.DAY_OF_MONTH));  
System.out.println(now.get(ChronoField.DAY_OF_YEAR));   
System.out.println(now.get(ChronoField.HOUR_OF_DAY));   
System.out.println(now.get(ChronoField.MINUTE_OF_HOUR));  
System.out.println(now.get(ChronoField.YEAR));  
System.out.println(now.get(ChronoField.DAY_OF_WEEK));  
System.out.println(now.get(ChronoField.MONTH_OF_YEAR));
```

### A seguir os métodos de comparação de java.time. Vale ressaltar que:
- Existe o método isEqual e equals por serem objetos;
- Existe o método isSupported que irá verificar se o tipo de TemporalField é suportado pela classe, por exemplo, sabemos que a classe LocalDate não possui o método getHourOfDay mas se tentarmos forçar a barra com o método get, da seguinte forma irá lançar java.time.temporal.UnsupportedTemporalTypeException: Unsupported field: HourOfDay
- Existe isBefore e isAfter

### Alteração das datas
- devemos ter em mente que sempre são resultados imutáveis, por tanto ele retorna um novo objeto com os valores alterados. 
- Também devemos ter em mente a mesma pegadinha que nos outros caso, como java.time.LocalTime não tem Mês, se tentamos chamar o método withMonth não irá compilar, pois "LocalTime does not have a day of month field".
- Existe também os métodos plus e minus que precisamos especificar qual unidade queremos alterar, para isto se utiliza uma enum chamada ChronoUnit. Lembrando sempre que se não tiver a unidade o código irá lançar UnsupportedTemporalTypeException
- Também podemos chamar o método específico, por exemplo plusDays. Lembrando sempre que se não tiver a unidade o código não irá compilar.

### A CLASSE MONTHDAY Atenção:
A classe MonthDay não possui nenhum método para somar ou subtrair unidades de tempo, logo, ela não tem nenhum método plus ou minus , e também não possui um método isSupported que receba ChronoUnit .

### Conversão de data:
- Podemos converter para LocalDate/LocalTime um LocalDateTime com o método toLocalDate ou toLocalTime respetivamente.
- Podemos converter pedaços de datas para um LocalDateTime com o método LocalTime.of(12, 0).atDate(LocalDate) e LocalDate.of(2015, 05, 5).atTime(LocalTime)


## Trabalhando com a data Legada.
### O exemplo a seguir converte uma java.util.Date em LocalDateTime , usando a timezone padrão do sistema:
```java
Date d = new Date(); 
Instant i = d.toInstant(); 
System.out.println(i);
LocalDateTime ldt1 = LocalDateTime.ofInstant(i, ZoneId.systemDefault());
Calendar c = Calendar.getInstance();
Instant i2 = c.toInstant();
LocalDateTime ldt2 = LocalDateTime.ofInstant(i2, ZoneId.systemDefault());
```

### Podemos fazer o caminho de volta:
```java
Date d = new Date();
Instant i = d.toInstant();
LocalDateTime ldt1 = LocalDateTime.ofInstant(i, ZoneId.systemDefault());
Instant i2 = ldt1.toInstant(ZoneOffset.UTC);
d = Date.from(i2);
```

### Perid e Duration são instantes de tempo e permitem fazer operações de este:
```java
Instant i = Instant.now();
Instant i2 =  Instant.EPOCH;
long seconds = Duration.between(i, i2).getSeconds();
```

### ChronoUnit é uma das classes mais versáteis, pois permite ver a diferença entre duas datas em várias unidades de tempo:
```java
LocalDate birthday = LocalDate.of(1983, 7, 22); 
LocalDate base = LocalDate.of(2014, 12, 25); 
// 31 years total 
System.out.println(ChronoUnit.YEARS.between(birthday, base)); 
// 377 months total 
System.out.println(ChronoUnit.MONTHS.between(birthday, base)); 
// 11479 days total 
System.out.println(ChronoUnit.DAYS.between(birthday, base));
```

### Extraindo data com Period
```java
LocalDate aniversario = LocalDate.of(2015, 11, 20);
LocalDate agora = LocalDate.now();
Period idade = Period.between(aniversario, agora);
idade.getUnits().forEach(u -> System.out.println( u + "->" + idade.get(u)));
```		

### Formatando uma data para string e de string para data
```java
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
System.out.println(LocalDate.of(1986, 04, 23).format(formatter));
System.out.println(LocalDate.parse("23/04/1986", formatter).format(formatter));
```

## Lambda

### Interfaces funcionais são interfaces normais, mas com apenas um método. Podemos verificar se a interface é funcional com a annotation @FunctionalInterface, se esta não for, o código não compila. 

### Regras para se escrever um lambda:
- Lambdas podem ter vários argumentos, como um método. Basta separá-los por , . 
- O tipo dos parâmetros pode ser inferido e, assim, omitido da declaração. 
- Se não houver nenhum parâmetro, é necessário incluir parênteses vazios, como em: Runnable r = () -> System.out.println("a runnable objec t!"); 
- Se houver apenas um parâmetro, podemos omitir os parênteses, como em: Predicate<Person> matcher = p -> p.getAge() >= 18; 
- O corpo do lambda pode conter várias instruções, assim como um método. Se houver apenas uma instrução, podemos omitir as chaves, como em: Predicate<Person> matcher = p -> p.getAge() >= 18; Se houver mais de uma instrução, é necessário delimitar o corpo do lambda com chaves, como em: Runnable r = () -> { int a = 10; int b = 20; System.out.println(a + b); }

### Regras de escopo
- Variaveis definidas localmente não poderão ser acessada dentro de uma expressão lambda a não ser que elas sejam final OU effectively final
```java
int unchangedLocalVar = 3; // effectively final
final int localVarFinal = 4; // final
int simpleLocalVar = 0;
// updated the value
simpleLocalVar = 9;
new Thread(() -> {
	System.out.println(unchangedLocalVar); // can read
	System.out.println(localVarFinal); // can read
	System.out.println(simpleLocalVar); // compile error
}).start();
```
- As variáveis do lambda são do mesmo escopo que o método onde ele foi declarado, portanto, não podemos declarar nenhuma variável, como parâmetro ou dentro do corpo, cujo nome conflite com alguma variável local do método:
```java
private void test(String param) {
	String methodVar = "method"; // not final
	Predicate<String> a = param -> param.length() > 0; // compile error
	Predicate<String> b = methodVar -> methodVar.length() > 0; // compile error
	Predicate<String> c = newVar -> newVar.length() > 0; // ok
}
```


### Simulados:

- Um array pode ser inicializado vazio, exemplo int[] as = {}
- Um integer irá ser inicializado com null
- Override = Herança, 
- Overload, mesmo nome, diferentes parâmetros
- Na estrutura switch se declaramos uma variavel da seguinte forma final int i; i = 0; e utilizamos no case o código não irá compilar (Pode usar parênteses nos cases)
- Alta coesão é bom e amplo acomplamento é ruim para dar manutenção
- ArrayList tem um construtor que passa um parâmetro int e quando printado qualquer arraylist,sempre sai bonitinho 
- NullPointerException é uma UncheckedException


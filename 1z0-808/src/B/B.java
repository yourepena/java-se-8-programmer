package B;

import A.A;

class Baap {
	public int h = 4;

	public int getH() {
		System.out.println("Baap " + h);
		return h;
	}
}

public class B extends Baap {
	public int h = 44;

	public int getH() {
		System.out.println("Beta " + h);
		return h;
	}

	public static void main(String[] args) {
		Baap b = new B();
		System.out.println(b.h + " " + b.getH());
		B bb = (B) b;
	System.out.println(bb.h + " " + bb.getH());
	}
}
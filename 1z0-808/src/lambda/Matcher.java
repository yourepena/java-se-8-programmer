package lambda;

@FunctionalInterface
public interface Matcher<T> {
	boolean test(T t);
}